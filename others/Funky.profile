[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Funky-console
Font=FiraCode Nerd Font Mono,12,-1,5,50,0,0,0,0,0
LineSpacing=0
UseFontLineChararacters=true

[Cursor Options]
CursorShape=2
CustomCursorColor=255,0,0
UseCustomCursorColor=true

[General]
Command=/usr/bin/fish
Icon=konsole
Name=Funky
Parent=FALLBACK/
TerminalCenter=true
TerminalColumns=110
TerminalMargin=0

[Interaction Options]
AutoCopySelectedText=true
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=1
HistorySize=512

[Terminal Features]
BlinkingCursorEnabled=true
